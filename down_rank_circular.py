from py2neo import Node, Relationship, Graph
from random import choice, randint
graph = Graph("bolt://neo4j:trust@localhost:7687")
graph.delete_all()

# Create community Persons:
i=0
for i in range(10):
    x = randint(5, 40)
    a = Node("Person", "community", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Transactions:
nodes = tuple(graph.nodes.match("Person", "community"))
fraud_node = Node("Person", "fraud", Id=i, rank=0, wallet=300)
i += 1
b = nodes[-1]
for a in nodes:
    ab = Relationship(a, "Pays", b, amount=4)
    a_fraud_node = Relationship(a, "Pays", fraud_node, amount=1)
    graph.create(ab)
    graph.create(a_fraud_node)


# Create Non-community Persons:
for i in range(3):
    x = randint(1, 40)
    a = Node("Person", "non-community", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Non-community Transactions:
nodes = graph.nodes.match("Person", "non-community")
for i in range(5):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Create inter community Transactions:
community_nodes = graph.nodes.match("Person", "community")
non_community_nodes = graph.nodes.match("Person", "non-community")

for i in range(1):
    a = choice(tuple(community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(non_community_nodes))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

for i in range(1):
    a = choice(tuple(non_community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(community_nodes))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)


# down-rank fraud:
nodes = graph.nodes.match("Person", "community")
for node in nodes:
    node["rank"] -= 1
    graph.push(node)

fraud_node["rank"] -= 1
graph.push(fraud_node)
