from py2neo import Node, Relationship, Graph
from random import choice, randint
graph = Graph("bolt://neo4j:trust@localhost:7687")
graph.delete_all()

# Create community Persons:
for i in range(1, 10):
    x = randint(1, 40)
    a = Node("Person", "community", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Transactions:
nodes = graph.nodes.match("Person")
for i in range(1, 100):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Create Non-community Persons:
for i in range(3):
    x = randint(1, 40)
    a = Node("Person", "non-community", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Non-community Transactions:
nodes = graph.nodes.match("Person", "non-community")
for i in range(5):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Create inter community Transactions:
community_nodes = graph.nodes.match("Person", "community")
non_community_nodes = graph.nodes.match("Person", "non-community")

for i in range(1):
    a = choice(tuple(community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(non_community_nodes))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)


# Create Out
out_node = Node("OUT", name="OUT")
graph.create(out_node)

# Create Out Transactions:
nodes = graph.nodes.match("Person")
for i in range(1, 5):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", out_node, amount=x)
    graph.create(ab)

for i in range(1, 10):
    a = choice(tuple(nodes))
    x = randint(1, 10)
    ab = Relationship(out_node, "Pays", a, amount=x)
    graph.create(ab)

# Up-rank community:
for node in community_nodes:
    node["rank"] += 1
    graph.push(node)


