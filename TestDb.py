from py2neo import Node, Relationship, Graph
from random import choice, randint
graph = Graph("bolt://neo4j:trust@localhost:7687")
graph.delete_all()
# a = Node("Person", Id=id, GD_wallet=gd_wallet, GD_redeam=gd_redeem,
#          GD_in=gd_in, GD_out=dg_out,
#          GD_conv_in=gd_conv_in,GD_conv_out=gd_conv_out)

# Create Persons:
for i in range(1, 10):
    x = randint(1, 40)
    a = Node("Person", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Transactions:
nodes = graph.nodes.match("Person")
for i in range(1, 100):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Up-rank Top earners:
nodes = graph.nodes.match("Person")
for node in nodes:
    rels = graph.match((node, ), r_type="Pays")
    payed = 0
    for rel in rels:
        payed += rel["amount"]
    node["payed"] = payed
    graph.push(node)

nodes = graph.nodes.match("Person").order_by("_.payed DESC").limit(3)
for node in nodes:
    node["rank"] += 1
    graph.push(node)



