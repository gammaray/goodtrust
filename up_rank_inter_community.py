from py2neo import Node, Relationship, Graph
from random import choice, randint
graph = Graph("bolt://neo4j:trust@localhost:7687")
graph.delete_all()

# Create inter-communities:
for i in range(5):
    x = randint(1, 40)
    a = Node("community", "inter-community", "community_{}".format(i), Id=i, rank=0, wallet=x)
    graph.create(a)

# Create Transactions:
community_nodes = graph.nodes.match("community")
for i in range(50):
    a = choice(tuple(community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(community_nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Create non-inter-communities:
for i in range(3):
    x = randint(1, 40)
    a = Node("community", "non_inter_community", Id=i, rank=0, wallet=x)
    graph.create(a)

# Create non_inter_community Transactions:
nodes = graph.nodes.match("community", "non_inter_community")
for i in range(5):
    a = choice(tuple(nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(set(nodes)-{a}))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Create non_inter_community an inter_community Transactions:
non_community_nodes = graph.nodes.match("community", "non_inter_community")
community_nodes = graph.nodes.match("community", "inter-community")
for i in range(1):
    a = choice(tuple(community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(non_community_nodes))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

for i in range(1):
    a = choice(tuple(non_community_nodes))
    if a["wallet"] < 1:
        continue
    b = choice(tuple(community_nodes))
    x = randint(1, a["wallet"])
    ab = Relationship(a, "Pays", b, amount=x)
    graph.create(ab)

# Up rank inter-communities
inter_community_nodes = graph.nodes.match("community", "inter-community")
for node in inter_community_nodes:
    node["rank"] += 1
    graph.push(node)


